import pyodbc
import pandas as pd
import yfinance as yf

server = 'asifdro-pc'
database = 'predict10'
username = 'asif'
password = 'asif'
cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
cursor = cnxn.cursor()

s_name ='TSLA'

df=yf.Ticker('TSLA').history( start="2021-05-14", end="2021-05-18", interval='1m')

# Insert Dataframe into SQL Server:
for index, row in df.iterrows():
    dict= row.to_dict()
    s_dt=index
    s_open=row['Open']
    s_high=row['High']
    s_low=row['Low']
    s_close=row['Close']
    s_volume=row['Volume']
    cursor.execute("INSERT INTO [dbo].[stoks_data]([s_name] ,[s_dt],[s_open] ,[s_high],[s_low],[s_close],[s_volume]) VALUES (?,?,?,?,?,?,?)" ,s_name, s_dt, s_open, s_high, s_low, s_close, s_volume)
cnxn.commit()
cursor.close()

