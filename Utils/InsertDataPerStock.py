import yfinance as yf
from Utils.Queries import InsertStockDfToSQLSERVER
import numpy as np
import pandas as pd
from datetime import *


def InsertDataPerStock (stock, start_t, end_t, interval_t):
    df=yf.Ticker(stock).history( start=start_t, end=end_t, interval=interval_t)
    InsertStockDfToSQLSERVER(df,stock)

def InsertDataPerStock_WithoutEndTime (stock, start_t, interval_t):
    df=yf.Ticker(stock).history( start=start_t, interval=interval_t)
    start_t=pd.to_datetime(start_t+timedelta(hours = 4), utc=True).tz_convert('America/New_York')
    df = df[df.index>start_t]
    InsertStockDfToSQLSERVER(df,stock)
