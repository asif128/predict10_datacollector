Create Database [predict10]

USE [predict10]
GO


CREATE TABLE [dbo].[stocks_data](
	[s_id] [int] IDENTITY(1,1) NOT NULL,
	[s_name] [nvarchar](128) NULL,
	[s_dt] [datetime] NULL,
	[s_open] [decimal](18, 4) NULL,
	[s_high] [decimal](18, 4) NULL,
	[s_low] [decimal](18, 4) NULL,
	[s_close] [decimal](18, 4) NULL,
	[s_volume] [decimal](18, 4) NULL,
	[db_dt] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [NonClusteredIndex-20210524-165527]    Script Date: 5/24/2021 4:57:08 PM ******/
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20210524-165527] ON [dbo].[stocks_data]
(
	[s_name] ASC,
	[s_dt] ASC
)
INCLUDE([s_id],[s_open],[s_high],[s_low],[s_close],[s_volume],[db_dt]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
ALTER TABLE [dbo].[stocks_data] ADD  CONSTRAINT [DF_stocks_data_db_dt]  DEFAULT (getdate()) FOR [db_dt]
GO

CREATE TABLE [dbo].[stocks](
	[name] [NVARCHAR](128) NULL
) ON [PRIMARY]
GO

INSERT [dbo].[stocks] ([name]) VALUES (N'TSLA')
GO
INSERT [dbo].[stocks] ([name]) VALUES (N'AAPL')
GO
INSERT [dbo].[stocks] ([name]) VALUES (N'FB')
GO
INSERT [dbo].[stocks] ([name]) VALUES (N'MSFT')
GO
INSERT [dbo].[stocks] ([name]) VALUES (N'AMZN')
GO
INSERT [dbo].[stocks] ([name]) VALUES (N'GOOGL')
GO
INSERT [dbo].[stocks] ([name]) VALUES (N'GOOG')
GO
INSERT [dbo].[stocks] ([name]) VALUES (N'JPM')
GO
INSERT [dbo].[stocks] ([name]) VALUES (N'JNJ')
GO
INSERT [dbo].[stocks] ([name]) VALUES (N'PG')
GO
INSERT [dbo].[stocks] ([name]) VALUES (N'V')
GO
INSERT [dbo].[stocks] ([name]) VALUES (N'NVDA')
GO
INSERT [dbo].[stocks] ([name]) VALUES (N'PYPL')
GO
INSERT [dbo].[stocks] ([name]) VALUES (N'NFLX')
GO


