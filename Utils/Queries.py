from Utils.LoadConfig import GetDBConfig
import pyodbc
from datetime import datetime
from Utils.colors import *


def ConenctToDB():
    server, database, username, password = GetDBConfig()
    cnxn = pyodbc.connect(
        'DRIVER={SQL Server};SERVER=' + server + ';DATABASE=' + database + ';UID=' + username + ';PWD=' + password)
    cursor = cnxn.cursor()
    return (cnxn, cursor)

def GetStocksList():
    cnxn, cursor = ConenctToDB()
    results = cursor.execute("SELECT name FROM dbo.stocks")
    stocks = list()


    for  row in results:
        stocks.append(row[0])

    return stocks

def DeleteStocksData_AllStock():
    cnxn, cursor = ConenctToDB()
    cursor.execute("TRUNCATE TABLE [dbo].[stocks_data]")
    cnxn.commit()
    cursor.close()
    print(bcolors.HEADER + 'All Data Deleted' + bcolors.ENDC)


def DeleteStocksData_PerStock(stock):
        cnxn, cursor = ConenctToDB()
        cursor.execute("Delete [dbo].[stocks_data] where s_name =?", stock)
        cnxn.commit()
        cursor.close()

def GetMaxDateInDB_PerStock(stock):
    cnxn, cursor = ConenctToDB()

    results = cursor.execute("SELECT MAX([s_dt]) FROM [dbo].[stocks_data] WHERE s_name =?", stock)
    ls = list()

    count=0
    for row in results:
        count +=1
        ls.append(row[0])

    if(row[0]==None):
        time = datetime.strptime('2010-01-01 00:00:00', '%Y-%m-%d %H:%M:%S')


    else:
       time =  ls[0]

    return time

def InsertStockDfToSQLSERVER (df,s_name):
    # Insert Dataframe into SQL Server:
    cnxn, cursor = ConenctToDB()
    for index, row in df.iterrows():
        print(row,s_name)
        dict = row.to_dict()
        s_dt = index
        s_open = row['Open']
        s_high = row['High']
        s_low = row['Low']
        s_close = row['Close']
        s_volume = row['Volume']
        if not(s_dt == 'Nan' or s_open == 'Nan' or s_high == 'Nan' or s_low =='Nan' or s_close == 'Nan' or s_volume == 'Nan'):
            cursor.execute(
            "INSERT INTO [dbo].[stocks_data]([s_name] ,[s_dt],[s_open] ,[s_high],[s_low],[s_close],[s_volume]) VALUES (?,?,?,?,?,?,?)", s_name, s_dt, s_open, s_high, s_low, s_close, s_volume)
    cnxn.commit()
    cursor.close()
