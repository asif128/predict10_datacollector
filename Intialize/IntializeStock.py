from Utils.Queries import GetMaxDateInDB_PerStock
from Utils.InsertDataPerStock import InsertDataPerStock
from datetime import *



def IntializeStock(stock):
    #get last data in DB. use as start_date in InsertDataPerStock.
    max_date = GetMaxDateInDB_PerStock(stock)
    stream_until=datetime.now()-timedelta(days = 3)
    while (max_date<stream_until):
        # gnrate next datetime period. use as end_Date in InsertDataPerStock.
        if(max_date>datetime.now()-timedelta(days = 29)):
            interval ='1m'
            next_period = max_date + timedelta(days=7)
        elif (max_date > datetime.now() - timedelta(days=60)):
            interval = '1h'
            next_period = max_date + timedelta(days=7)
        elif(max_date>datetime.now()-timedelta(days = 729)):
            interval ='1h'
            next_period = max_date + timedelta(days=60)
        else :
            interval = '1d'
            next_period = max_date + timedelta(days=365)

        InsertDataPerStock(stock, max_date, next_period, interval)
        max_date = next_period




