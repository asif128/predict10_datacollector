from Utils.Queries import GetMaxDateInDB_PerStock
from Utils.InsertDataPerStock import InsertDataPerStock_WithoutEndTime
from datetime import *



def StreamStock(stock):
    #get last data in DB. use as start_date in InsertDataPerStock.
    max_date = GetMaxDateInDB_PerStock(stock)
    # gnrate next datetime period. use as end_Date in InsertDataPerStock.
    interval ='1m'
    InsertDataPerStock_WithoutEndTime(stock, max_date, interval)




