from Utils.Queries import *
from Intialize.IntializeStock import *
from LiveStreaming.StreamStock import *
from datetime import *
import time
import sys


def StreamAllStocks():
    while(1==1):
        try:
            now = datetime.now()
            stocks_list = GetStocksList()
            for stock in stocks_list:
                StreamStock(stock)
            while (datetime.now()<now+timedelta(minutes = 1)):
                time.sleep(1)
        except Exception as e:
            print(bcolors.FAIL + 'Stram Data Failed!!!' + bcolors.ENDC)
            print('--------------------')
            print(bcolors.FAIL + str(e)+ bcolors.ENDC)
            print(bcolors.WARNING + 'Retray more 60 seconds...' + bcolors.ENDC)
            time.sleep(60)

